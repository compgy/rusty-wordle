use rand::prelude::IteratorRandom;
use std::collections::HashSet;
use std::fmt;
use std::hash::Hash;
use std::io::Write;
use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
#[derive(Eq, Hash )]
enum LetterStatus {
    Correct(char),
    InWord(char),
    Wrong(char),
}
// not sure whether I really need all this but it doesn't compile without it
impl PartialEq<LetterStatus> for LetterStatus {
    fn eq(&self, other: &LetterStatus) -> bool {
        match (self, other) {
            (LetterStatus::Correct(c1), LetterStatus::Correct(c2)) => c1 == c2,
            (LetterStatus::InWord(c1), LetterStatus::InWord(c2)) => c1 == c2,
            (LetterStatus::Wrong(c1), LetterStatus::Wrong(c2)) => c1 == c2,
            _ => false,
        }
    }
}

impl fmt::Display for LetterStatus {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut stdout = StandardStream::stdout(ColorChoice::Always);
        match self {
            LetterStatus::Correct(s) => {
                stdout
                    .set_color(ColorSpec::new().set_fg(Some(Color::Green)))
                    .unwrap();
                write!(&mut stdout, "{} ", s).unwrap();
            }
            LetterStatus::InWord(s) => {
                stdout
                    .set_color(ColorSpec::new().set_fg(Some(Color::Yellow)))
                    .unwrap();
                write!(&mut stdout, "{} ", s).unwrap();
            }
            LetterStatus::Wrong(s) => {
                stdout
                    .set_color(ColorSpec::new().set_fg(Some(Color::Red)))
                    .unwrap();
                write!(&mut stdout, "{} ", s).unwrap();
            }
        }
        Result::Ok(())
    }
}


const FILENAME: &str = "/Users/alessio/Coding/rusty-wordle/src/word_list";

fn main() {
    println!("Welcome to Rusty Wordle :)");
    let solution_word = find_word();

    let mut current_word = String::new();
    let mut guessed_letters: HashSet<LetterStatus> = HashSet::new();
    for _i in 0..5 {
        loop {
            println!("Please enter a five letter word:");
            // clear string because read_line appends
            current_word.clear();

            io::stdin()
                .read_line(&mut current_word)
                .expect("Failed to read word");
            if !validate_word(current_word.trim()) {
                println!("Word not in list!");
                continue;
            } else {
                break;
            }
        }
        let letter_match = match_letters(&current_word.trim(), &solution_word,);
        print_word(&letter_match);
        // &matches is a macro which uses match under the hood
        // it is used because with a normal == I cannot ignore the value with _
        if letter_match.iter().all(|l| matches!(l, LetterStatus::Correct(_))) {
            println!("You win!");
            break;
        }
        guessed_letters.extend(letter_match);
        print_guesses(&guessed_letters);
    }
}
fn print_guesses(guesses: &HashSet<LetterStatus>) {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);
    write!(&mut stdout, "Guesses: ").unwrap();
    guesses.iter().for_each(|g| print!("{}", &g));
    print!("\n");
    stdout.reset().unwrap();
}

// not perfect refactoring but it's alright for now
fn print_word(word: &Vec<LetterStatus>) {
    let mut stdout = StandardStream::stdout(ColorChoice::Always);
    // TODO: sort hashset
    word.iter().for_each(|g| print!("{}", &g));
    print!("\n");
    stdout.reset().unwrap();
}

fn find_word() -> String {
    let f = File::open(&FILENAME)
        // | | is a lambda
        .unwrap_or_else(|e| panic!("(;_;) file not found: {}: {}", FILENAME, e));
    let f = BufReader::new(f);

    let lines = f.lines().map(|l| l.expect("Couldn't read line"));

    lines
        .choose(&mut rand::thread_rng())
        .expect("File had no lines")
}

fn validate_word(word: &str) -> bool {
    let reader = BufReader::new(
        File::open(FILENAME)
            .unwrap_or_else(|e| panic!("(;_;) file not found: {}: {}", FILENAME, e)),
    );

    let reader = reader.lines().peekable();

    let mut is_valid = false;
    for ln in reader {
        if ln.unwrap_or_else(|e| panic!("cannot read line {}", e)) == *word.to_ascii_lowercase() {
            is_valid = true;
        }
    }
    println!("{}", &is_valid);
    is_valid
}

fn match_letters(entered_word: &str, solution_word: &str) -> Vec<LetterStatus> {
    let mut letter_statuses: Vec<LetterStatus> = Vec::with_capacity(5);
    let words = entered_word.chars().zip(solution_word.chars());
    words.for_each(|c| {
        letter_statuses.push({
            if c.0 == c.1 {
                LetterStatus::Correct(c.0.to_ascii_uppercase())
            } else if solution_word.contains(|f| f == c.0) {
                LetterStatus::InWord(c.0.to_ascii_uppercase())
            } else {
                LetterStatus::Wrong(c.0.to_ascii_uppercase())
            }
        });
    });
    letter_statuses
}
